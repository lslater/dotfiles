
" Install plug.vim
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Install plugins
call plug#begin('~/.local/share/nvim/plugged')

Plug 'terryma/vim-multiple-cursors'

call plug#end()

" For when I inevitably forget what to do, run ':PlugInstall' within vim to
" install any new plugs

set tabstop=4		" Make tabs a sensible size
set background=dark	" Set font colours appropriately for dark terminals

