" Disable compatability mode
set nocompatible

" Set tab and indentation size
set tabstop=4
set shiftwidth=4

" Automatic indentation
set autoindent
" set smartindent

" Set font colours appropriately for dark terminals
set background=dark	

" Line numbers
set number			

" Set @c macro to CSS-style comment the current line before moving to the beginning of the next
let @c="I/* \<C-o>$ */\<Esc>j0"
inoremap <C-c> <C-o>@c
vnoremap <C-c> @c
nnoremap <C-c> @c

" Set @h macro to HTML-style comment the current line before moving to the beginning of the next
let @h="I<!-- \<C-o>$ -->\<Esc>j0"
inoremap <C-h> <C-o>@h
vnoremap <C-h> @h
nnoremap <C-h> @h

" Set @u macro to remove block comments on the current line
let @u="^daw$daw"
inoremap <C-u> <C-o>@u
vnoremap <C-u> @u
nnoremap <C-u> @u

" PHP array pretty-printing
let @r="Iecho \"<pre>\".print_r(\<Esc>A,true).\"</pre>\";"

" Re-highlight selection after an indent
vnoremap < <gv
vnoremap > >gv

" Turn on filetype detection 
filetype plugin indent on

" Syntax highlighting
syntax on

" Set 80-line guide
"set tw=79
"set colorcolumn=80
"highlight ColorColumn ctermbg=233

" Allow the mouse to set the cursor location
" set mouse=a

" Remap up and down arrows to move lines visually
inoremap <Down> <C-o>g<Down>
vnoremap <Down> g<Down>
nnoremap <Down> g<Down>
inoremap <Up> <C-o>g<Up>
vnoremap <Up> g<Up>
nnoremap <Up> g<Up>

" Toggle paste mode
set pastetoggle=<F2>

" Ctrl+Return between characters (usually brackets) to put an indented
" cursor on a separate line between them
imap <C-Return> <CR><CR><C-o>k<Tab>

" Spellcheck
map <F6> :setlocal spell! spelllang=en_GB<CR>

" Ctrl a to select all
inoremap <C-a> <esc>ggVG
vnoremap <C-a> <esc><esc>ggVG
nnoremap <C-a> ggVG

" Ctrl+J between characters (usually brackets) to put an indented
" cursor on a separate line between them
imap <C-j> <CR><CR><C-o>k<Tab>

