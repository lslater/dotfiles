" Disable compatability mode
set nocompatible

" A dark theme
colorscheme slate

" Set tab and indentation size
set tabstop=4
set shiftwidth=4

" Automatic indentation
set autoindent
" set smartindent

" Set font colours appropriately for dark terminals
set background=dark	

" Line numbers
set number			

" Set @c macro to CSS-style comment the current line before moving to the beginning of the next
let @c="I/* \<C-o>$ */\<Esc>j0"
inoremap <C-c> <C-o>@c
vnoremap <C-c> @c
nnoremap <C-c> @c

" Set @h macro to HTML-style comment the current line before moving to the beginning of the next
let @h="I<!-- \<C-o>$ -->\<Esc>j0"
inoremap <C-h> <C-o>@h
vnoremap <C-h> @h
nnoremap <C-h> @h

" Re-highlight selection after an indent
vnoremap < <gv
vnoremap > >gv

" Turn on filetype detection 
filetype plugin indent on

" Syntax highlighting
syntax on

" Remap up and down arrows to move lines visually
inoremap <Down> <C-o>g<Down>
vnoremap <Down> g<Down>
nnoremap <Down> g<Down>
inoremap <Up> <C-o>g<Up>
vnoremap <Up> g<Up>
nnoremap <Up> g<Up>

" Toggle paste mode
set pastetoggle=<F2>

" Set Consolas font, can be useful on Windows
" set guifont=Consolas:h11

" Ctrl+Return/J between characters (usually brackets) to put an indented
" cursor on a separate line between them
imap <C-Return> <CR><CR><C-o>k<Tab>
imap <C-j> <CR><CR><C-o>k<Tab>

