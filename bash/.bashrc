# Resources:
# - https://www.key-shortcut.com/en/writing-systems/35-symbols/arrows/
# - https://www.howtogeek.com/307701/how-to-customize-and-colorize-your-bash-prompt/

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

HISTCONTROL=ignoreboth # Don't put duplicates or lines starting with space in history
shopt -s histappend # append to the history file, don't overwrite it
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# Uncomment for a colored prompt, if the terminal has the capability
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
		# We have color support; assume it's compliant with Ecma-48
		# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
		# a case would tend to support setf rather than setaf.)
		color_prompt=yes
    else
		color_prompt=
    fi
fi

function git_status() {
	branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1/')
	status=$(git status 2> /dev/null)
	behind=0
	ahead=0
	modified=0
	untracked_files=0

	# Set $changes and $untracked_files to 1 if appropriate
	if [[ $status == *"branch is ahead"* ]]; then
		ahead=1
	fi

	if [[ $status == *"branch is behind"* ]]; then
		behind=1
	fi

	if [[ $status == *"modified:"* ]]; then
		modified=1
	fi
	if [[ $status == *"Untracked files:"* ]]; then
		untracked_files=1
	fi

	printf "\001\e[00m\002"
	if [ "$branch" != "" ]; then
		printf "$branch"
	fi

	# Print coloured icons to show git status
	icons=""
	if [ $untracked_files -eq 1 ]; then
		icons="${icons} \001\e[0;31m\002+"
	fi
	if [ $modified -eq 1 ]; then
		icons="${icons} \001\e[0;33m\002~"
	fi
	if [ $behind -eq 1 ]; then
		icons="${icons} \001\e[0;35m\002↓"
	fi
	if [ $ahead -eq 1 ]; then
		icons="${icons} \001\e[0;35m\002↑"
	fi
	printf "${icons}"

	# Revert to no colour
	printf "\001\e[00m\002"
}

PROMPT_DIRTRIM=3
if [ "$color_prompt" = yes ]; then
	# Wrap non-printed information (such as colour info) in \[ and \] or \001 and \002
	# \033[COLOURm or \e[COLOURm is how to select a colour
	# Time: \[\e[1;33m\]\D{%H:%M}
	PS1='\n${debian_chroot:+($debian_chroot)}\[\e[00m\][\[\e[1;32m\]\u@\h \[\e[1;34m\]\w$(git_status)]\$ '
else
	PS1='\n${debian_chroot:+($debian_chroot)}[\u@\h \w$(git_status)]\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi


# Variable exports
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
export PATH="$PATH:$HOME/bin/"
export EDITOR="vim"


# aliases
alias la='ls --color=always -lAhF'
alias ll='ls --color=always -lhF'
alias l='ll'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Useful if you want to pipe STDOUT into the system clipboard
alias clip='xclip -selection clipboard'

alias yt='youtube-dl'
alias dl='youtube-dl'
alias ytdl='youtube-dl'

alias gst='git status'
alias gs='git status'
alias g='git status .'
alias gd='git diff'
alias ga='git add'
alias gc='git commit'

alias vim='vim -p'
alias vim='gvim -v'

alias update='sudo dnf -y upgrade && flatpak update -y'
alias upgrade='update'

alias drupull='git pull && ddev composer install && ddev drush deploy'

alias k='kubectl'
alias mainctl='kubectl -n greenwich-main-website'
alias gcdctl='kubectl -n gcd-website'

export GIT_NAME="Lee Slater"
#### PERSONAL KEYS ########################################
export PERSONAL_EMAIL="lee.slater@protonmail.com"
export GITLAB_PERSONAL_KEY="~/.ssh/gitlab:lslater"
export GITHUB_PERSONAL_KEY="~/.ssh/github:lslater"
#### WORK KEYS ############################################
export WORK_EMAIL="lee.slater@royalgreenwich.gov.uk"
export GITLAB_WORK_KEY="~/.ssh/gitlab:rbg_lslater"
export GITHUB_WORK_KEY="~/.ssh/github:rbg-lslater"
export CODEENIGMA_WORK_KEY="~/.ssh/ce:lslater"

alias gitlab-work='git config user.name $GIT_NAME && git config user.email $WORK_EMAIL && git config core.sshCommand "ssh -i $GITLAB_WORK_KEY"'
alias gitlab-personal='git config user.name $GIT_NAME && git config user.email $PERSONAL_EMAIL && git config core.sshCommand "ssh -i $GITLAB_PERSONAL_KEY"'

alias github-work='git config user.name $GIT_NAME && git config user.email $WORK_EMAIL && git config core.sshCommand "ssh -i $GITHUB_WORK_KEY"'
alias github-personal='git config user.name $GIT_NAME && git config user.email $PERSONAL_EMAIL && git config core.sshCommand "ssh -i $GITHUB_PERSONAL_KEY"'

alias codeenigma='git config user.name $GIT_NAME && git config user.email $WORK_EMAIL && git config core.sshCommand "ssh -i $CODEENIGMA_WORK_KEY"'

# git clone with a specified SSH key
# Run with `ssh-clone [rbg|ls] [url]`
function ssh-clone() {
	context=""
	remote=""
	case "$1" in
		rbg | w | work)
			context="WORK"
			;;
		ls | lslater | p | personal)
			context="PERSONAL"
			;;
	esac
	case "$2" in
		*codeenigma*)
			remote="CODEENIGMA"
			;;
		*gitlab*)
			remote="GITLAB"
			;;
		*github*)
			remote="GITHUB"
			;;
	esac

	key="${remote}_${context}_KEY"
	if [ -z ${!key} ]; then
		echo "$key is an invalid key"
		return 1;
	fi
	echo ${!key}

	git clone -c core.sshCommand="ssh -i ${!key}" $2
}

main="-n greenwich-main-website"
gcd="-n gcd-website"
int="-n intranet-forms"

# Login to AWS
function aws-login() {
	if [ -z $1 ]; then
		echo "Env needs to be set"
		return 1;
	fi
	aws sso login --profile $1 && aws eks update-kubeconfig --region eu-west-2 --name apps-$1 --profile $1
}

# Select namespace
function aws-namespace() {
	if [ -z $1 ]; then
		echo "Namespace needs to be set"
		return 1;
	fi
	case "$1" in
		main)
			namespace="greenwich-main-website"
			;;
		gcd)
			namespace="gcd-website"
			;;
		*)
			namespace=$1
			;;
	esac
	kubectl config set-context --current --namespace=$namespace
}

# SSH into AWS EKS via kubectl
function aws-ssh() {
	if [ -z $1 ]; then
		echo "Namespace needs to be set"
		return 1;
	fi
	case "$1" in
		main)
			namespace="greenwich-main-website"
			;;
		gcd)
			namespace="gcd-website"
			;;
		*)
			namespace=$1
			;;
	esac
	kubectl exec -it `kubectl get pods --namespace $namespace | awk '{print $1}' | sed -n '2p' `  --namespace $namespace -- /bin/bash
}

# Copy files to AWS container
function aws-put() {
	usage="Usage: aws-put <file>"
	if [ -z $1 ]; then
		echo $usage
		return 1;
	fi
	file=$1
	filename=$(basename -- "$file")
	kubectl cp $file `kubectl get pods | awk '{print $1}' | sed -n '2p'`:/opt/drupal/localgov-dist/$filename
}

# Copy files from an AWS container
function aws-get() {
	usage="Usage: aws-get <filename>"
	if [ -z $1 ]; then
		echo $usage
		return 1;
	fi
	filename=$1
	kubectl cp `kubectl get pods | awk '{print $1}' | sed -n '2p'`:/opt/drupal/localgov-dist/$filename $filename
}

function aws-get-db() {
	usage="Usage: aws-get-db <env> <namespace>"
	if [ -z $1 ]; then
		echo $usage
		return 1;
	fi
	if [ -z $2 ]; then
		echo $usage
		return 1;
	fi
	aws-login $1
	aws-namespace $2
	filename="$1_$2_$(date "+%Y-%m-%d_%H%M%S").sql"
	kubectl exec -it `kubectl get pods | awk '{print $1}' | sed -n '2p' ` -- /opt/drupal/localgov-dist/bin/drush sql:dump > $filename
	echo "Saved database to $filename"
}

function aws-put-db() {
	usage="Usage: aws-put-db <dev/tst> <filename>"
	if [ -z $1 ]; then
		echo $usage
		return 1;
	fi
	if [ -z $2 ]; then
		echo $usage
		return 1;
	fi
	env=$1
	filename=$2
	if [[ "$env" != "dev" && "$env" != "tst" ]]; then
		echo $usage
		return 1;
	fi
	aws-login $env
	aws-namespace main
	kubectl exec -i `kubectl get pods | awk '{print $1}' | sed -n '2p' ` -- /opt/drupal/localgov-dist/bin/drush sqlc < $filename
	kubectl exec `kubectl get pods | awk '{print $1}' | sed -n '2p' ` -- /opt/drupal/localgov-dist/bin/drush cr
}

function merge-to() {
	usage="Usage: merge-to <branch>"
	if [ -z $1 ]; then
		echo $usage
		return 1;
	fi
	if [ "$1" == "dev" ]; then
		echo "Merge to dev must be performed manually";
		return 1;
	fi
	git switch $1 && \
	git pull && \
	git merge --no-edit - && \
	git push && \
	git switch -
}

# Set vi bindings in bash
set -o vi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# Added by LM Studio CLI (lms)
export PATH="$PATH:/home/lslater/.cache/lm-studio/bin"
