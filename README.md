# .dotfiles #

Requires `stow` (or manually unpacking).

Use `stow <pkg>` to load the dotfiles of a particular package into $HOME. `./stow-common` is provided for commonly-used packages.

