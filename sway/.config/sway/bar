#!/usr/bin/sh

volume=$(pactl list sinks | awk '$1=="Volume:" {print $5}')

# Produces "21 days", for example
uptime_formatted=$(uptime | cut -d ',' -f1  | cut -d ' ' -f4,5)

# e.g. Thu 27/01/2022 14:15
date_formatted=$(date +'%a %d/%m/%Y %H:%M')

# Get the Linux version but remove the "-1-ARCH" part
linux_version=$(uname -r | cut -d '-' -f1)

# Show the battery percentage
bat_max=$(cat /sys/class/power_supply/BAT0/charge_full)
bat_now=$(cat /sys/class/power_supply/BAT0/charge_now)
((bat_one_percent=$bat_max/100))
((bat_percent=$bat_now/$bat_one_percent))
battery_percent="$bat_percent%"

# Returns the battery status: "Full", "Discharging", or "Charging".
battery_status=$(cat /sys/class/power_supply/BAT0/status)

# Emojis and characters for the status bar
# Requires noto-fonts-emoji
# 💎 💻 💡 🔌 ⚡ 📁 \| 🔊🐧
echo "$volume 🔊  $uptime_formatted ↑  $battery_percent 🔋  $date_formatted "
